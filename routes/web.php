<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome')->with('var', 'test');
//});

//Route::get('/', function () {
//    return view('welcome', ['var'=>'test']);
//});

//Route::get('/', function () {
//    $var = "test";
//    return view('welcome', compact('var'));
//});
//Route::get('/users', function () {
//    return "test";
//});
//Route::get('/{id}', function ($id) {
//    return $id;
//});
//Route::get('/{id?}', function ($id = "") {
//    return $id;
//});
//Route::get('/{id}', function ($id) {
//    return $id;
//})->where('id','[0-9]+');

//Route::get('/{id}', function ($id) {
//    return $id;
//})->where('id','[0-9]');

//Route::get('/{username}', function ($username) {
//    return $username;
//})->where('username','[A-Za-z]+');
//Route::get('/member/{id}',"MembersControllers@show")->name('members.show');
//Route::middleware('auth')->group(function (){
//
//    Route::get('dashboard',function (){
//        return view('dashboard');
//    });
//    Route::get('account',function (){
//        return view('account');
//    });
//});

//Route::prefix('dashboard')->group(function (){
//
//    Route::get('/',function (){
//        return 'dashboard';
//    });
//
//    Route::get('menu',function (){
//        return 'dashboard/menu';
//    });
//
//});
//Route::group(['prefix'=>'dashboard'],function (){
//
//    Route::get('/',function (){
//        return 'dashboard';
//    });
//
//    Route::get('menu',function (){
//        return 'dashboard/menu';
//    });
//
//});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//Route::domain('test.myApp.com')->group(function (){
//    Route::get('/',function (){
//        return "hi";
//    });
//});
//
//Route::domain('{account}.myApp.com')->group(function (){
//    Route::get('users/{id}',function ($account, $id){
//        return "hi";
//    });
//});

//Route::namespace('Dashboard')->group(function (){
//    Route::get('Dashboard/test','TestController@index');
//});

//Route::name('admin.')->group(function (){
//    Route::get('users',function (){
//
//    })->name('users');
//});
// Route::fallback(function (){
//    redirect('/');
// });

//Route::resource('/category','CategoryController');

//Route::get('redirect-with-helper',function (){
//    return redirect()->to('category/create');
//});
//
//Route::get('redirect-with-helper-short',function (){
//    return redirect('category');
//});
//
//Route::get('redirect-with-facades',function (){
//    return Redirect::to('category');
//});

//Route::redirect('redirect-by-route','category');
//
//Route::get('redirect',function (){
//    return redirect()->route('category.create',['id'=>2]);
//});
//
//Route::get('redirect-back',function (){
//    return redirect()->back();
//});
//
//Route::get('redirect-action',function (){
//    return redirect()->action('CategoryController@create');
//});

//Route::get('/category',function (){
//    return redirect('category/create')->with('status','Enable');
//});
//Route::get('/category','CategoryController@index');
//Route::post('something-you-can-not-do',function (){
//    abort_unless(Auth::User()->isAdmin(),403);
//    abort_if(!Auth::User()->isAdmin(),403);
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/',function (){
//   return response('hi', 200)->header('Content-Type','text/plain');
//});

//Route::get('/',function (){
//   return response('hi', 200)
//       ->header('Content-Type','text/plain')
//       ->cookie('name','value',$minutes);
//});

Route::get('/','HomeController@index');
